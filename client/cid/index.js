import App from './components/app';
import Header from './components/header';
import Listings from './components/listings';
import Car from './components/car';

import Services from './services';

export default angular.module('cid', [
    'cid.templates',
    'ngTouch',
    'ngMaterial',

    Services.name,

    App.name,
    Header.name,
    Listings.name,
    Car.name,
])
