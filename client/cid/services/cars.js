import { randInt, randElement } from 'utils';

function fakeCar() {
    return {
        price: randInt(500, 75000),
        year: randInt(1995, 2015),
        make: randElement(['Honda', 'Toyota', 'Ford', 'BMW', 'Hyundai', 'GM', 'Lexus']),
        model: randElement(['Taurus', 'Civic', 'Accent', 'Camry', 'Prius', 'Cavalier', 'Focus']),
        body: randElement(['Slim', 'Slender', 'Curvy']),
        trim: randElement(['10', '20', '30']),
        odometer: randInt(25000, 150000),
        condition: randElement(['shit', 'bad', 'okay', 'good', 'new']),
        color: randElement(['black', 'brown', 'red', 'white', 'silver', 'blue', 'yellow'])
    }
}


export default /*@ngInject*/ function($http) {
    let service = {};

    service.fake = function(count) {
        let a = [];
        for(let i = 0; i < count; i++) {
            a.push( fakeCar() );
        }
        return a;
    };

    service.fetch = function(start = 0, count = 10) {
        return $http.get('/api/cars', {
            params: {
                start,
                count
            }
        })
            .then((res) => res.data );
    };

    service.count = function() {
        return $http.get('/api/cars/count');
    };

    return service;
}