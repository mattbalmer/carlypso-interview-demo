import cars from './cars';

export default angular.module('cid.services', [])
    .service('cars', cars);
