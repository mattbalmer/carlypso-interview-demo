export default class Controller {

    MIN_OPTIONS = [
        { label: 'None', value: undefined },
        250,
        500,
        1000,
        2000,
        3000,
        5000
    ];
    MAX_OPTIONS = [
        { label: 'None', value: undefined },
        250,
        500,
        1000,
        2000,
        3000,
        5000
    ];

    _subhead = null;
    sort = {};

    /*@ngInject*/ constructor($scope, $rootScope, $timeout) {
        this.$timeout = $timeout;
        this.$rootScope = $rootScope;

        $scope.$watch('ctrl.filters', (filters) => {
            if(filters) $rootScope.$broadcast('filters.updated', filters)
        }, true);

        $scope.$on('newCarCount', (e, count) => {
            this.carCount = count;
        });
    }

    toggleSubhead(subhead) {
        if(this._locked == true) return;
        this._locked = true;

        if(this._subhead == subhead)
            this._subhead = null;
        else
            this._subhead = subhead;

        this.$timeout(function() {
            this._locked = false;
        }.bind(this), 100);
    }

    toggleSort(method) {
        if(this.sort.method == method) {
            if(this.sort.mode < 2)
                this.sort.mode++;
            else
                this.sort = {};
        } else {
            this.sort = {
                method,
                mode: 1
            }
        }

        this.$rootScope.$broadcast('sort.updated', this.sort);
    }
}