import directive from './directive';

export default angular.module('header', [])
    .directive('cidHeader', directive)