export default class Controller {

    PAGE_COUNT = 10;

    sort = {};
    filters = {};
    cars = [];
    filteredCars = [];

    /*@ngInject*/ constructor($scope, $rootScope, cars) {
        this.carService = cars;

        this.$rootScope = $rootScope;

        $scope.$on('filters.updated', (e, filters) => {
            this.filters = filters;
            this.filteredCars = this.filter(this.cars);

            this.onCarsUpdated();
        });

        $scope.$on('sort.updated', (e, sort) => {
            this.sort = sort;
            this.filteredCars = this.sortCars(this.filteredCars);
        });

        this.carService.fetch()
            .then(this.appendCars.bind(this));

        this.onCarsUpdated();
    }

    more() {
        this.carService.fetch(this.cars.length, this.PAGE_COUNT)
            .then(this.appendCars.bind(this));
    }

    appendCars(cars) {
        this.cars = this.cars.concat(cars);

        let filteredCars = this.filter(cars);
        this.filteredCars = this.filteredCars.concat(filteredCars);

        this.onCarsUpdated();
    }

    onCarsUpdated() {
        this.filteredCars = this.sortCars(this.filteredCars);

        let carCount = this.filteredCars.length;
        this.$rootScope.$broadcast('newCarCount', carCount);
    }

    filter(cars) {
        let filters = this.filters;

        let filter = (car) => {
            if(filters.min && car.price < filters.min)
                return false;

            if(filters.max && car.price > filters.max)
                return false;

            return true;
        };

        return cars.filter(filter);
    }

    sortCars(cars) {
        let sort = this.sort;

        if(!sort) return cars;

        let sortFn = (a, b) => {
            if(sort.method == 'price') {
                return sort.mode == 2 ? a.price - b.price : b.price - a.price;
            }
        };

        return cars.sort(sortFn);
    }
}