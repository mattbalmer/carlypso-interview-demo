import controller from './controller';

export default function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        templateUrl: 'cid/components/listings/template.html',
        controller,
        controllerAs: 'ctrl',
        bindToController: true
    }
};