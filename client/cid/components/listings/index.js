import directive from './directive';

export default angular.module('listings', [])
    .directive('cidListings', directive)