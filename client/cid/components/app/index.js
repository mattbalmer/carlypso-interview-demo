import directive from './directive';

export default angular.module('app', [])
    .directive('cidApp', directive)