export default function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        templateUrl: 'cid/components/app/template.html'
    }
};