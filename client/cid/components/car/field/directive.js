export default function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            label: '@'
        },
        templateUrl: 'cid/components/car/field/template.html'
    }
};