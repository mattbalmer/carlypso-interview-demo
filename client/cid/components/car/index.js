import directive from './directive';
import fieldDirective from './field/directive';

export default angular.module('car', [])
    .directive('cidCar', directive)
    .directive('cidCarField', fieldDirective)