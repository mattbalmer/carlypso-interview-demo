import controller from './controller';

export default function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        templateUrl: 'cid/components/car/template.html',
        controller,
        controllerAs: 'ctrl',
        bindToController: {
            car: '='
        }
    }
};