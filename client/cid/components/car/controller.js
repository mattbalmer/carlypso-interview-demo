import { randElement } from 'utils';

export default class Controller {

    /*@ngInject*/ constructor() {
        this.image = randElement(['taurus.jpg', 'prius.png', 'ferari.jpg', 'infiniti.jpg']);
    }
}