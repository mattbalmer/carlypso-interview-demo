import express from 'express';
import cars from './cars';

let routes = express.Router();

routes.use('/cars', cars);

export default routes;