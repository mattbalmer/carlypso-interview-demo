import express from 'express';
import http from 'superagent';
let cars = express.Router();

cars.get('/', (req, res) => {
    let start = req.query.start || 0;
    let count = req.query.count || 10;

    http
        .get(`http://interview.carlypso.com/listings?offset=${start}&limit=${count}`)
        .end((err, _res) => {
            if(err) {
                res.status(500).send(err);
            } else {
                res.status(_res.status).json(_res.body.value);
            }
        })
});

cars.get('/count', (req, res) => {
    http
        .get('http://interview.carlypso.com/count')
        .end((err, _res) => {
            if(err) {
                res.status(500).send(err);
            } else {
                res.status(_res.status).json(_res.body.value);
            }
        })
});

export default cars;