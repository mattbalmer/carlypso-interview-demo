import path from 'path';
import express from 'express';
import routes from './router';

let server = express();

server.use( routes );

server.use( express.static( path.join(__dirname, '../static') ) );

server.listen(3000, function() {
    console.log('Server listening on port 3000');
});