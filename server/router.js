import express from 'express';
import api from './api';

let routes = express.Router();

routes.use('/api', api);

export default routes;