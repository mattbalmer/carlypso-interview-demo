# carlypso interview demo

Demo project for carlypso interview

## Installation

    $ npm install

(This will also install bower components)

## Running the project

    $ npm start

Then browse to `http://localhost:3000`

(Note: on Windows machines, just run `node ./server/index.js` instead)