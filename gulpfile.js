var gulp = require('gulp');

var recipes = {
    ngm: require('./gulp/recipes/ng-modules')({
        input: 'client',
        output: 'static/'
    }),
    ng_css: require('./gulp/recipes/ngm-css'),
    ng_js: require('./gulp/recipes/ngm-js'),
    ng_html: require('./gulp/recipes/ngm-html')
};

gulp.task('ng:css', recipes.ngm(recipes.ng_css));
gulp.task('ng:html', recipes.ngm(recipes.ng_html));
gulp.task('ng:js', recipes.ngm(recipes.ng_js));
gulp.task('ng', ['ng:css', 'ng:html', 'ng:js']);

gulp.task('watch', function() {
    return gulp.watch('src/**/*', ['ng']);
});

gulp.task('compile', ['ng']);
gulp.task('default', ['compile', 'watch']);