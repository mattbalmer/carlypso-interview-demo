var fs = require('fs'),
    path = require('path'),
    merge = require('merge-stream');

module.exports = function(paths) {
    var modules = fs.readdirSync(paths.input);

    return function(gulptask) {
        var tasks = modules.map(function(moduleName) {
            var input = path.join(paths.input, moduleName),
                output = paths.output;

            return gulptask(input, output, moduleName);
        });

        var stream = merge.apply(merge, tasks);

        return function() {
            return stream;
        };
    }
};