var gulp = require('gulp'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    annotate = require('gulp-ng-annotate'),
    sourcemaps = require('gulp-sourcemaps'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    through2 = require('through2');

module.exports = function(input, output, moduleName) {
    return gulp.src(input + '/index.js')
        .pipe(through2.obj(function(file, enc, next) {
            browserify(file.path, {debug: true})
                .transform(babelify.configure({
                    optional: ['es7.classProperties']
                }))
                .bundle(function(err, res) {
                    if(err) {return next(err);}

                    file.contents = res;
                    next(null, file);
                });
        }))
        .on('error', function(error) {
            console.log(error.stack);
            this.emit('end');
        })
        .pipe(rename(moduleName + '.js'))
        .pipe(annotate({ add: true, remove: true, single_quotes: true }))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(output + '/js/'));
};