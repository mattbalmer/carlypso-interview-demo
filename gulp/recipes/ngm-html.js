var gulp = require('gulp'),
    concat = require('gulp-concat'),
    ngHtml2Js = require('gulp-ng-html2js'),
    minifyHtml = require('gulp-minify-html'),
    wrap = require('gulp-wrap'),
    jade = require('gulp-jade'),
    gulpif = require('gulp-if');

module.exports = function(input, output, moduleName) {
    return gulp.src([ input + '/**/*.jade', input + '/**/*.html'])
        .pipe(gulpif(/\.jade$/, jade()))
        .pipe(minifyHtml({
            comments: true,
            spare: true,
            quotes: true
        }))
        .pipe(ngHtml2Js({
            declareModule: false,
            rename: function(url) {
                return url.replace('.jade', '.html');
            },
            prefix: moduleName + '/',
            template:
                "    $templateCache.put('<%= template.url %>', '<%= template.escapedContent %>');\n"
        }))
        .pipe(concat(moduleName + '.templates.js'))
        .pipe(wrap(
            "angular.module('<%= moduleName %>', []).run(['$templateCache', function($templateCache) {\n" +
            "<%= contents %>" +
            "}])"
            , { moduleName: moduleName + '.templates' }
        ))
        .on('error', function(error) {
            console.log(error.stack);
            this.emit('end');
        })
        .pipe(gulp.dest(output + '/js'));
};