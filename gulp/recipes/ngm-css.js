var gulp = require('gulp'),
    concat = require('gulp-concat'),
    stylus = require('gulp-stylus'),
    nib = require('nib');

module.exports = function(input, output, moduleName) {
    return gulp.src(input + '/**/*.styl')
        .pipe(stylus({
            use: nib(),
            'import': [ 'nib' ]
        }))
        .pipe(concat(moduleName + '.css'))
        .pipe(gulp.dest(output + '/css'));
};