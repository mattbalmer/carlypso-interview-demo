function randInt(min, max) {
    return Math.round( Math.random() * (max-min) + min );
}
function randElement(a) {
    return a[randInt(0, a.length - 1)];
}

export { randInt, randElement };